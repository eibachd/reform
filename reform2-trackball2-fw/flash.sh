#!/bin/bash

echo "Flash MNT Reform Trackball2 (RP2040):"

select yn in "Flash" "Exit"; do
    case $yn in
        Flash ) picotool load build/reform2-trackball2-fw.uf2;;
        Exit ) exit;;
    esac
done

